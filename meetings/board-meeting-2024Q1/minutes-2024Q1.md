# Redox OS Board Meeting 2024Q1

The Redox OS Board Meeting for 2024Q1 took place on January 23, 2024 at 11:20am MST (UTC-7).
A video of the meeting is posted on [the Redox OS YouTube channel](https://www.youtube.com/watch?v=J5bLxAWzdUE).

## Attendance

- Ron Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present
- Jacob Schneider, director-nominee: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2023Q4 were accepted. The minutes are available in the [nonproft repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit/-/blob/main/meetings/board-meeting-2023Q4/minutes-2023Q4.md).

## Election of Directors

As required by the bylaws, Redos OS non-profit elects its directors and officers each year.
An officer is someone who performs duties on behalf of the board while a director is a voting member of the board.
In our case Redox officers are also directors, so as we elect our officers we are also electing them as members of the board.
In particular, the president presides over the board meetings and the vice-president acts in the president's place if the president is unable to attend.

Resolved: Ronald Williams was elected president and director.
Resolved: Jeremy Soller was elected vice-president and director.
Resolved: Alberto Souza was elected as secretary and director.
Resolved: Jeremy Soller was elected treasurer.
Resolved: Jacob Schneider was elected director.

## 2023 Finances

Jeremy presented an informal summary of the finances.

During 2023, we began transitioning income and expenses to the Redox OS nonprofit. This report reflects that transition.

| Item                        |    Amount |
| :-------------------------- | --------: |
| Income                      |           |
| Initial Donation            | 10,000.00 |
| Patreon (Feb-Dec)           |  6,747.79 |
| T-Shirts                    |    242.99 |
| Gross Income                | 16,990.78 |
|                             |           |
| Expenses                    |           |
| Community Manager (Oct-Dec) |  1,200.00 |
| Filings                     |     50.00 |
| Hosting (Oct-Dec)           |    345.60 |
| Gross Expenses              |  1,595.60 |
|                             |           |
| Net Income for 2023         | 15,395.18 |
|                             |           |
| Bank Balance                | 15,967.39 |
| Teespring/PayPal            |    242.99 |
| Credit Card                 |  (815.20) |
| Net Assets at Dec. 31, 2023 | 15,395.18 |

A formal report will be presented at the next board meeting.

## 2024 Budget

Ron presented the budget for 2024.

These are the assumptions that were made:

- No new source of donations is included.
They will be added to the budget as the donations are received.
- The funding for the trademark application has been carried forward
from 2023, and increased slightly.
It is still only an estimate based on some preliminary research.
- There are two Redox Summer of Code projects included in this budget.
If we can get our projects directly funded,
for example by Google Summer of Code, we may not use that money.

|                        | Current | Monthly |  Annually  |
| :--------------------- | ------: | ------: | ---------: |
| Expenses	                                              |
| Server Hosting         |         |     120 |     1440   |
| Community Manager      |         |     400 |     4800   |
| Misc.                  |         |         |      500   |
| Legal and Trademark    |         |         |     5000   |
| Accounting and Filings |         |         |      500   |
| Redox Summer of Code   |         |         |    11000   |
| **Expenses Total**     |         |         |  **23240** |
|                                                         |
| Income		                                          |
| Patreon                |         |     600 |     7200   |
| T-Shirts               |         |      30 |      360   |
| Other Donations/Grants TBD			                  |
| **Income Total**       |         |         |   **7560** |
|                                                         |
| **Deficit**            |         |         | **-15680** |
|                                                         |
| Bank Balance           |  16,249 |         |            |
| Credit Card            |     915 |         |            |
| **Final Balance**      |  15,334 |         |   **-346** |


Resolved: The budget was adopted as presented, and the treasurer is authorized to make expenditures according to the budget.

## Fundraising

There was a broad discussion of fundraising. Some of the commentary is summarized here.

- German Sovereign Tech Fund has a minimum proposal value of 150,000 Euros, but is not currently open for proposals.
- Other funds have different sizes.
- We need to create a prioritized list of projects of various sizes that we can use as proposals for funding, and we need Jeremy's input as a key part of that.
- We need to do active mentoring of projects. Ron is available to do mentoring within his areas of knowledge.
- Google Summer of Code is mainly for people who are new to open source or new to projects, so we should not set expectations too high for GSoC projects.
- Bug bounties would be a way of engaging people in bite size projects.
- Only a small percentage of views will become donors or buy merch.
- We need to communicate on more platforms to increase views.

###

- We need to contact hardware vendors to start the conversation, to get them comfortable with the idea of funding us.
- We need to build relationships with vendors, starting at the lower levels and working our way up to people who can authorize larger donations.
- We need an "Introducing Redox" video with Jeremy giving an overview of Redox architecture and current capabilities. Perhaps we could work with a podcaster to do it as a scripted interview.
- Showing the Cosmic apps would provide some flashiness.
- We also need to have some technical content about how Redox is different from Linux.
- Alberto plans to do a demonstration video showing various things running.

###

- We need to increase the number of views and give people more opportunities to donate or buy merch.
- Donations come with few strings attached, so it's a good source of income.
- Corporate donations will come with strings, so we will need to allocate developers to do what those donors request, rather than focusing on our own priorities.
- We need more news posts, point releases, and videos to try to draw viewers and reach a larger audience.
- We have Phoronix reviewing our news posts, it's one of the largest Linux news sites.
- For the demo, HD video playback is not smooth, and there are other technical issues that will impact the demo video.

###

- We need to consider accepting cryptocurrency donations.
- It would be good to have information from potential donors about whether their ability to donate would be affected by having a particular cryptocurrency available. Bitcoin seems to be the most popular. Ethereum is another important option, but there are concerns about Tornado Cash being sanctioned. Monero is potentially of concern because of Tornado Cash.
- We have an opportunity with with Radicle/Drips funding for Termion. We would need an Ethereum wallet, and the funds would be received as Drips or USDC. However, we do not have anyone who has volunteered to take responsibility for Termion.
- People are generally not using Tornado Cash due to the sanctions, so there is low probability that we need to worry about sanctions in this case.

###

- Jeremy received a donation of $86,000 in 2022 and a portion of that still remains. Jeremy plans to donate $10,000 per year of the remaining amount to cover Redox Summer of Code projects.
- We are hoping to generate a stable income stream for the nonprofit so we can target having a full time employee.
- Continuity of knowledge is important due to the complexity of Redox.
- Increasing views and merch sales can help get us to the point where we can hire a full time engineer.
- It's important to have a new release to show how much compatibility we have via relibc, and all the software that we have been porting.
- Cosmic and other visual elements will do a lot to increase enthusiasm.

###

- The FAQ and Book improvements have helped to attract developers.
- Improving the visual aspects of the website and making Redox more visually appealing are important to increasing interest in the project.
- We need to upgrade the website. The current site is based on the Rust website from 2015.
- Jacob has been working on a website for his company that may be reusable at some level.
- We should try to fix the static sytle definitions we have to make them less outdated.
- We need to consider both serving the website from Redox, and viewing the website in a browser on Redox.
- We should avoid JavaScript.
- Jacob took an action item to coordinate the conversation on the website, collecting ideas.

Resolved: Jacob will lead the conversation on the website improvements.

## Nonprofit Repository

Resolved: Jeremy will create a public repo for the Nonprofit, and Alberto will coordinate moving the documents to the public repo.

## Agenda for the Next Board Meeting

- Review and approve the minutes from this meeting
- Report on fundraising, promotion and outreach
- Review fundraising plan
- Report on trademark
- Review financial report for 2023

## Adjournment

The meeting was adjourned at 12:46pm MST.
