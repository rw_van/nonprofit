# Redox OS 2023Q2 Board Meeting

The Redox OS 2023Q2 board meeting took place on June 21, 2023 at 12:00 noon MDT (UTC-6).

## Attendance

Ronald Williams - present
Jeremy Soller - present
Alberto Souza - present

## Temporary Chair and Secretary

Resolved: Ronald Williams was appointed temporary chair for the meeting.
Resolved: Alberto Souza was appointed secretary for the meeting.

## Report on Articles of Incorporation

Jeremy reports that the Redox OS nonprofit is incorporated and registered in Colorado, and the articles of incorporation are in his possession.

Resolved: The secretary of the corporation was directed to ensure that a copy of the signed and certified articles 

## Adoption of Bylaws

Resolved: The Redox OS Bylaws were adopted as the bylaws of the corporation. (Document Reference TBD)

## Conflict of Interest Policy

Resolved: The Redox OS Conflict of Interest policy was adopted as the conflict of interest policy of the corporation. (Document Reference TBD)

## Appointment of Officers

Resolved: Ronald Williams was appointed board president and director.

Resolved: Jeremy Soller was appointed board vice-president and director.

Resolved: Alberto Souza was appointed board secretary and director.

Resolved: Jeremy Soller was appointed treasurer.

## Principal Office

Resolved: The principal office was agreed upon.

## Report on Finances

Jeremy Soller reported that the Redox OS bank account is at Chase Bank. Redox OS has $12,800 in it's bank account.

The Redox Patreon is now making payments to the Redox OS account.

Jeremy Soller has provided Redox OS with a loan of $10,000.

Resolved: The Redox OS bank account at Chase Bank is the authorized bank account for the corporation.

## Expenditures

Resolved: The treasurer is the signatory for the corporate bank accounts.

Resolved: Until a budget is approved by the board, expenditures must be approved by a minimum of two board members.

## File for Tax Exemption

Resolved: The application for tax exempt status under Section 501(c)(4) of the Internal Revenue Code is authorized.

## Adjournment

The meeting was adjourned at 12:30pm MDT.