# Preparation for Director's Meeting

## Action Items to be presented at this board meeting
- [ ] President: Propose fundraising plan for 2023
- [ ] President: Propose plan for trademarking name, logos, BDFL title, management of redox-os.org
- [ ] Treasurer: Present budget for 2023, including projected funds and use of funds
- [ ] Treasurer: Present 2023 plan for integration of past donations
- [ ] Treasurer/Secretary: Add Board to Articles of Incorporation
- [ ] Secretary: Confirm books and records are as expected e.g. confirmed via video
- [ ] Secretary: Provide minutes of this meeting prior to the next meeting, so they can be adopted by the board

## Additional action items
- [ ] Treasurer: Propose accounting plan
- [ ] President: Propose plan for public input, complaints about executives/board, whistleblowers
- [ ] Secretary & Treasurer: Propose plan for public on-line records, including public visibility of finances and corporation documents