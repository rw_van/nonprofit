# Redox OS Board Meeting 2023Q4

The Redox OS Board Meeting for 2023Q4 took place on Dec. 29, 2023 at 2pm MST (UTC-7).
A video of the meeting will be posted on YouTube at a future date.

## Attendance

- Ron Williams, president: present
- Jeremy Soller, vice president and treasurer: present
- Alberto Souza, secretary: present

## Minutes from last meeting

Resolved: The minutes from the Redox OS Board Meeting 2023Q3 were accepted.
The minutes may be in the [nonprofit-private repository on GitLab](https://gitlab.redox-os.org/redox-os/nonprofit-private/-/blob/main/meetings/board-meeting-2023Q3/minutes-2023Q3.md).

## Trademark

No action has been taken on trademark as of yet.

## Financial Status

- Cash balance: $16,067.39
- Teespring balance: $192.99
- Credit card balance: $515.20
- Monthly Patreon income has declined slightly, it was $549.88 for November.
- Monthly expenses: $515.20.
- The $4,000 that was budgeted for trademark fees will not be spent this calendar year and will be pushed into 2024.

## T-Shirt Sales

We have sold 14 t-shirts at full price, of which 7 of them were purchased by Ron.
- T-shirt profit to date: $192.99
- Almost all shirts sold were black

## Fundraising

Discussion of fundraising was deferred.

## Board of Directors

We have our Board of Directors elections due to take place in January.
We currently have 3 directors. Ideally we should have 4 directors.
It also gives us the ability to have a board meeting even if one person is not available.
Also, as the meeting chairperson, I should remain neutral at board meetings as much as possible,
and only vote on key items or for tie breaking.

Ron, Jeremy and Alberto plan to volunteer to continue as board members.

Ron: As for identifying a fourth board member, I propose the following criteria.
It should be someone who:
- is known to Jeremy
- has some association with Redox, preferably as a contributor
- can attend minimal but regular board meetings and participate in board discussions
- can provide their time on the board without compensation

Resolved: Upon motion duly moved, seconded and carried, the president is instructed to post a request for volunteers for the board on Matrix.

## Agenda for Next Board Meeting

- Review of the minutes of this meeting
- Board of Directors Elections
- 2023 Financial Report
- 2024 Budget
- Plan for fundraising

## Adjournment

The meeting was adjourned at 2:10pm MDT.