# Board Meeting Script 2023Q4

## Preamble

President: Welcome to the Redox OS board meeting for Q4 of 2023.

## Call to order

President: I call the meeting of the Board of Directors of Redox OS to order.

## Attendance

Ron: When I state your name please acknowledge your presence.

- Ron Williams: Yes
- Jeremy Soller: Yes
- Alberto Souza: Yes

President: We have a quorum.

## Minutes from last meeting

Ron: The minutes from the previous board meeting have been provided to you. Are there any corrections?

Ron: If there are no corrections...

Ron: The minutes are approved as distributed.

## Trademark

Ron: In the last meeting, there was an action to investigate lawyers for obtaining a trademark. Is there anything to report?

Jeremy: Nothing to report this quarter.

## Financial Status

Ron: Jeremy, could we have a report on the financial status for this quarter?

Jeremy: Our bank balance is $16,067.39
We have a balance of $192.99 at Teespring.
Our credit card balance is $515.20
Our monthly Patreon income has declined slightly, it was $549.88 for November.
Our monthly expenses are $515.20.
The $4,000 that was budgeted for trademark fees will not be spent this calendar year and will be pushed into 2024.

## T-Shirt Sales

Ron: We have started our t-shirt sales. So far, we have sold 14 t-shirts at full price, of which 7 of them were purchased by me.
There's a profit of $192.99 to date. Excluding my purchases, 6 of the t-shirts sold were black and one was grey.
Feedback seems to be that people really like the black shirts, rather than people not liking the light shirts.

## Fundraising

Ron: I have distributed a draft fundraising plan for 2024. Can I get a motion to discuss?

Jeremy: Moved.

Alberto: Seconded.

Ron: [Discussion of fundraising plan]

Ron: Can I get a motion that the members of the board review and revise the fundraising plan for discussion at the next board meeting?

Jeremy: Moved.

Alberto: Seconded

Ron: In favor say Aye, opposed say no.

In favor - Aye. Opposed - No.

Ron: Upon motion duly moved and carried, the fundraising plan will be reviewed and revised by the board members for discussion at the next board meeting.

## Board of Directors

Ron: Can I have a motion to discuss our upcoming board meeting?

Jeremy: Moved.

Alberto: Seconded.

Ron: We have our Board of Directors elections due to take place in January.
We currently have 3 directors. Ideally we should have 4 directors.
It also gives us the ability to have a board meeting even if one person is not available.
Also, as the meeting chairperson, I should remain neutral at board meetings as much as possible,
and only vote on key items or for tie breaking.

Ron: Do you both plan to volunteer for the board for 2024?

Jeremy: Yes.

Alberto: Yes.

Ron: As do I.

Ron: As for identifying a fourth board member, I propose the following criteria.
It should be someone who:
- is known to Jeremy
- has some association with Redox, preferably as a contributor
- can attend board meetings and participate in board discussions
- can provide their time on the board without compensation

Ron: Is there anything anyone would like to add or comment on regarding the board elections?

[Open Discussion]

Ron: If there is no further discussion, could I get a motion to instruct the president post a request for volunteers for the board on Matrix?

Jeremy: Moved.

Alberto: Seconded.

Ron: Those in favor say Aye, opposed say No.

In favor - Aye. Opposed - No.

Ron: Upon motion duly moved, seconded and carried, the president is instructed to post a request for volunteers for the board on Matrix.

## Agenda for Next Board Meeting

Ron: The following items are proposed for the next board meeting, to take place before January 31.

- Review of the minutes of this meeting
- Board of Directors Elections
- 2023 Financial Report
- 2024 Budget
- Plan for fundraising

## Adjournment

Ron: Is there any other business for the board to consider at this time?

Discussion.

Jeremy: I move to adjourn.

Alberto: Seconded.

Ron: It is moved and seconded that we adjourn. Those in favor say "Aye", those opposed say "No".

In favor - Aye. Opposed - No.

This meeting is adjourned.