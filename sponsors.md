# Sponsors

This document will cover our current sponsor candidates, corporate sponsors and sponsorship programs.

### Candidates

We keep the sponsors that we plan to contact on [this](https://gitlab.redox-os.org/redox-os/nonprofit/-/issues/1) tracking issue.

### Corporate

- Nothing yet

### Programs

- Nothing yet