# Redox OS Nonprofit Financial Report for 2023

These are the unaudited financial statements for the Redox OS nonprofit, for the period February 1, 2023 to December 31, 2023. This is the first Financial Report for the Redox OS nonprofit.

### Statement of Financial Position

For the year ended December 31, 2023

|                                           |         2023 |
| ----------------------------------------- | -----------: |
| **ASSETS**                                |              |
| &nbsp; &nbsp; Cash and Cash Equivalents   |    15,967.39 |
| &nbsp; &nbsp; Accounts Receivable         |       242.99 |
| **TOTAL ASSETS**                          |    16,210.38 |
|                                           |              |
| **LIABILITIES AND NET ASSETS**            |              |
| **Liabilities**                           |              |
| &nbsp; &nbsp; Accounts Payable            |       815.20 |
| **Total Liabilities**                     |       815.20 |
| **Net Assets**                            |              |
| &nbsp; &nbsp; Without Donor Restrictions  |    15,395.18 |
| &nbsp; &nbsp; With Donor Restrictions     |            - |
| **Total Net Assets**                      |    15,395.18 |
| **TOTAL LIABILITIES AND NET ASSETS**      |    16,210.38 |
|                                           |              |

----

### Statement of Activities

For the year ended December 31, 2023

|                                       |              |         2023 |              |
| :------------------------------------ | -----------: | :----------: | -----------: |
|      | Without Donor<br/>Restrictions | With Donor<br/>Restrictions | Total        |
| **REVENUE**                           |              |              |              |
| &nbsp; &nbsp; Grants and Donations    |    16,747.79 |            - |    16,747.79 |
| &nbsp; &nbsp; Merchandise Net         |       242.99 |            - |       242.99 |
| **TOTAL REVENUE**                     |    16,990.78 |            - |    16,990.78 |
|                                       |              |              |              |
| **EXPENSES**                          |              |              |              |
| &nbsp; &nbsp; Programs                |     1,545.60 |            - |     1,545.60 |
| &nbsp; &nbsp; Management and General  |        50.00 |            - |        50.00 |
| **TOTAL EXPENSES**                    |     1,595.60 |            - |     1,595.60 |
|                                       |              |              |              |
| **CHANGE IN NET ASSETS**              |    15,395.18 |            - |    15,395.18 |
| **NET ASSETS, START OF YEAR**         |         0.00 |              |         0.00 |
| **NET ASSETS, END OF YEAR**           |    15,395.18 |              |    15,395.18 |

----

### Statement of Cash Flow

For the year ended December 31, 2023

|                                               |         2023 |
| :-------------------------------------------- | -----------: |
| **CASH FLOWS FROM OPERATING ACTIVITIES**      |              |
| &nbsp; &nbsp; Change in net assets            |    15,395.18 |
| &nbsp; &nbsp; Change in liabilities           |       815.20 |
| **NET CASH PROVIDED BY OPERATING ACTIVITIES** |    16,210.38 |
|                                               |              |
| **NET INCREASE IN CASH AND CASH EQUIVALENTS** |    16,210.38 |
| **CASH AND CASH EQUIVALENTS, START OF YEAR**  |            - |
| **CASH AND CASH EQUIVALENTS, END OF YEAR**    |    16,210.38 |

----

### Statement of Functional Expenses

For the year ended December 31, 2023

|                                       |          |  2023 |             |             |
| :------------------------------------ | -------: | ----: | ----------: | ----------: |
|              | Programs | Fundraising | Management<br/>and General     |       Total |
| &nbsp; &nbsp; Contracts and Stipends  | 1,200.00 |     - |             |    1,200.00 |
| &nbsp; &nbsp; Cloud Services          |   345.60 |     - |           - |      345.60 |
| &nbsp; &nbsp; Other Expenses          |        - |     - |       50.00 |       50.00 |
| **TOTAL FUNCTIONAL EXPENSES**         | 1,545.60 |     - |       50.00 |    1,595.60 |