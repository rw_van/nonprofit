Redox OS Nonproft 2024 Budget

|                        | Current | Monthly |  Annually  |
| :--------------------- | ------: | ------: | ---------: |
| Expenses	                                              |
| Server Hosting         |         |     120 |     1440   |
| Community Manager      |         |     400 |     4800   |
| Misc.                  |         |         |      500   |
| Legal and Trademark    |         |         |     5000   |
| Accounting and Filings |         |         |      500   |
| Redox Summer of Code   |         |         |    11000   |
| **Expenses Total**     |         |         |  **23240** |
|                                                         |
| Income		                                          |
| Patreon                |         |     600 |     7200   |
| T-Shirts               |         |      30 |      360   |
| Other Donations/Grants TBD			                  |
| **Income Total**       |         |         |   **7560** |
|                                                         |
| **Deficit**            |         |         | **-15680** |
|                                                         |
| Bank Balance           |  16,249 |         |            |
| Credit Card            |     915 |         |            |
| **Final Balance**      |  15,334 |         |   **-346** |
